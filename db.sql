INSERT INTO story (id, title, summary, author, age_min, age_max, duration) VALUES (3,'L écoute aux portes', 'Un fabuleux album de CLaude Ponti', 'Claude Ponti', 2, 6, 3.4);

SELECT * FROM story;

INSERT INTO tag (id, story_id) VALUES (1, 1),(2, 1), (3, 2), (4, 2), (5, 2), (6, 1);

SELECT * FROM tag;

INSERT INTO "user" (id, email, roles, password, firstname, lastname) VALUES (2, 'test@test.com', '1234', 'John', 'Doe');

SELECT * FROM "user";

SELECT * FROM user_story;