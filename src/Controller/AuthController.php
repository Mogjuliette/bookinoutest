<?php

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController extends AbstractController
{
    public function __construct(private UserRepository $repo) {
    }
    #[Route('/api/users', methods: 'POST')]
    public function register(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher): Response
    {
        $user = $serializer->deserialize($request->getContent(), User::class, 'json');
        //On vérifie s'il n'y a pas déjà un user avec cet email en base de données
        if($this->repo->findOneBy(['email' => $user->getEmail()])) {
            return $this->json(['error' => 'User already exists'], Response::HTTP_BAD_REQUEST);
        }
        //On hash son mot de passe (symfony rajoute automatiquement sel et probablement poivre)
        $hashedPassword = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hashedPassword);
        //On lui assigne un rôle par défaut
        $user->setRoles(['ROLE_USER']);

        $this->repo->save($user, true);

        return $this->json($user);
    }
}
